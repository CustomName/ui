package testoproject.ui;

import java.util.HashMap;

public class Context {

    private HashMap<String, String> data = new HashMap<>();

    public void set(String key, String value){
        data.put(key, value);
    }

    public String get(String key){
        return data.get(key);
    }
}
