package testoproject.ui.steps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.ru.И;
import io.cucumber.java.ru.Тогда;
import testoproject.ui.pages.MarketPage;

import java.util.stream.Stream;

public class FilterSteps {

    private MarketPage marketPage;

    public FilterSteps(MarketPage marketPage){
        this.marketPage = marketPage;
    }

    @И("разворачиваем фильтр {string}")
    public void openByType(String type){
        marketPage.filter().openByType(type);
    }

    @Тогда("заполняем фильтр Цена от {string} до {string}")
    public void fillPrice(String from, String to){
        marketPage.filter().fillPrice(from, to);
    }

    @Тогда("заполняем фильтр Производитель {string}")
    public void fillFilterProducer(String values) {
        Stream.of(values.split(",")).forEach(value -> {
            marketPage.filter().checkProducer(value.trim());
        });
    }

    @И("нажимаем Показать подходящие")
    public void clickApply() {
        marketPage.filter().clickApply();
    }
}
