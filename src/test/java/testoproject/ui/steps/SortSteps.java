package testoproject.ui.steps;

import io.cucumber.java.ru.Когда;
import testoproject.ui.pages.MarketPage;

public class SortSteps {

    private MarketPage marketPage;

    public SortSteps(MarketPage marketPage){
        this.marketPage = marketPage;
    }

    @Когда("выбираем сортировку {string}")
    public void getTitleProduct(String type){
        marketPage.sortElement().sortBy(type);
    }
}
