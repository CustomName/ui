package testoproject.ui.steps;

import io.cucumber.java.ru.Когда;
import io.cucumber.java.ru.Тогда;
import testoproject.ui.Context;
import testoproject.ui.pages.MarketPage;

public class ProductSteps {

    private MarketPage marketPage;

    private Context context;

    public ProductSteps(MarketPage marketPage, Context context){
        this.marketPage = marketPage;
        this.context = context;
    }

    @Тогда("проверяем кол-во продуктов на странице {int}")
    public void checkCountProduct(Integer expCount){
        marketPage.productPage().checkCountProduct(expCount);
    }

    @Когда("получаем заголовок продукта {int} и сохраняем в переменную {string}")
    public void getTitleProduct(Integer index, String var){
        context.set(var, marketPage.productPage().getProductTitle(index - 1));
    }

    @Тогда("проверить сортировку По цене")
    public void checkSortByPrice(){
        int countProduct = marketPage.productPage().getCountProduct();

        int prevPrice = marketPage.productPage().getProductPrice(0);

        for(int i = 1; i < countProduct; i++){
            int currPrice = marketPage.productPage().getProductPrice(i);
            assert currPrice >= prevPrice :
                    String.format("Цена продукта %d, %d меньше, чем %d", i, currPrice, prevPrice);

            prevPrice = currPrice;
        }
    }
}
