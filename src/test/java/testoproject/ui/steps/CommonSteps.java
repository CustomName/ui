package testoproject.ui.steps;

import io.cucumber.java.ru.Тогда;

import static com.codeborne.selenide.Selenide.title;

public class CommonSteps {

    @Тогда("проверить заголовок страницы {string}")
    public void checkTitle(String expTitle) {
        assert title().equals(expTitle) :
                String.format("Title | exp: %s | act: %s", expTitle, title());
    }
}
