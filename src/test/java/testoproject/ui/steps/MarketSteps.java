package testoproject.ui.steps;

import io.cucumber.java.ru.И;
import io.cucumber.java.ru.Когда;
import io.cucumber.java.ru.Тогда;
import testoproject.ui.Context;
import testoproject.ui.pages.MarketPage;

public class MarketSteps {

    private MarketPage marketPage;
    private Context context;

    public MarketSteps(MarketPage marketPage, Context context){
        this.marketPage = marketPage;
        this.context = context;
    }

    @Когда("выбираем категорию {string}")
    public void chooseCategory(String category) {
        marketPage.clickCategory(category);
    }

    @Когда("выбираем подкатегорию {string} в группе {string}")
    public void chooseSubCategory(String item, String group) {
        marketPage.clickSubCategory(group, item);
    }

    @Когда("выбираем все фильтры")
    public void chooseAllFilters() {
        marketPage.clickAllFilters();
    }

    @И("производим поиск {string}")
    public void search(String var){
        marketPage.search(context.get(var));
    }

    @Тогда("сравниваем заголовок продукта {int} с переменной {string}")
    public void compareTitleProduct(Integer index, String var){
        String actValue = marketPage.productPage().getProductTitle(index);
        String expValue = context.get(var);

        assert expValue.equals(actValue) :
                String.format("Title продукта | exp = %s | act = %s", expValue, actValue);
    }
}
