package testoproject.ui.steps;

import io.cucumber.java.ru.Дано;
import io.cucumber.java.ru.Когда;
import testoproject.ui.pages.MainPage;

import static com.codeborne.selenide.Selenide.open;

public class MainSteps {

    private MainPage mainPage;

    @Дано("открыть страницу {string}")
    public void openPage(String url) {
        mainPage = open(url, MainPage.class);
    }

    @Когда("переходим в {string}")
    public void iGoTo(String tab) {
        mainPage.clickTab(tab);
    }
}
