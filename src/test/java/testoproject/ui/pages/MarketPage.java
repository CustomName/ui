package testoproject.ui.pages;

import org.openqa.selenium.By;
import testoproject.ui.pages.elements.FilterElement;
import testoproject.ui.pages.elements.ProductElement;
import testoproject.ui.pages.elements.SortElement;

import static com.codeborne.selenide.Selenide.$;

public class MarketPage {

    private static final String EL_ALL_CATEGORIES = "//div[contains(@class, 'tab_type_navigation-menu-grouping')]";
    private static final String EL_CATEGORY = "//a/span[text()='%s']";
    private static final String EL_SUBCATEGORY_ALL = "//div/a[text()='%s']/../../div/span";
    private static final String EL_SUBCATEGORY = "//div/a[text()='%s']/../../div//a[text()='%s']";
    private static final String EL_ALL_FILTERS = "//a[contains(@href, 'filters')]";
    private static final String EL_INPUT_SEARCH = "header-search";
    private static final String EL_BTN_SUBMIT = "//button[@type='submit']";

    private FilterElement filterPage;
    private ProductElement productPage;
    private SortElement sortElement;

    public MarketPage(FilterElement filterPage, ProductElement productPage, SortElement sortElement){
        this.filterPage = filterPage;
        this.productPage = productPage;
        this.sortElement = sortElement;
    }

    public void clickCategory(String category){
        $(By.xpath(EL_ALL_CATEGORIES)).click();
        $(By.xpath(String.format(EL_CATEGORY, category))).click();
    }

    public void clickSubCategory(String group, String item){
        $(By.xpath(String.format(EL_SUBCATEGORY_ALL, group))).click();
        $(By.xpath(String.format(EL_SUBCATEGORY, group, item))).click();
    }

    public void clickAllFilters(){
        $(By.xpath(EL_ALL_FILTERS)).click();
    }

    public void search(String value){
        $(By.id(EL_INPUT_SEARCH)).sendKeys(value);
        $(By.xpath(EL_BTN_SUBMIT)).click();
    }

    public FilterElement filter(){
        return filterPage;
    }

    public ProductElement productPage(){
        return productPage;
    }

    public SortElement sortElement(){
        return sortElement;
    }
}
