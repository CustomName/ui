package testoproject.ui.pages;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class MainPage {

    public void clickTab(String tab){
        $(By.linkText(tab)).click();
    }
}
