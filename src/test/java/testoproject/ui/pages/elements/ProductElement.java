package testoproject.ui.pages.elements;

import org.openqa.selenium.By;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.codeborne.selenide.Selenide.$$;

public class ProductElement {

    private static final String EL_PRODUCTS = "//div[contains(@class,'n-snippet-cell2_type_product')]";
    private static final String EL_PRODUCT_TITLE = "//div[contains(@class,'n-snippet-cell2__title')]";
    private static final String EL_PRODUCT_PRICE = "//div[contains(@class,'n-snippet-cell2_type_product')]//div[@class='price']";

    public int getCountProduct(){
        return $$(By.xpath(EL_PRODUCTS)).size();
    }

    public void checkCountProduct(int expCount){
        int actCount = getCountProduct();

        assert actCount == expCount :
                String.format("Кол-во продуктов | exp = %d | act = %d", expCount, actCount);
    }

    public String getProductTitle(int index){
        return $$(By.xpath(EL_PRODUCTS)).get(index)
                                .findElement(By.xpath(EL_PRODUCT_TITLE)).getText();
    }

    public int getProductPrice(int index){
        int price = -1;

        String priceStr = $$(By.xpath(EL_PRODUCT_PRICE)).get(index).getText();

        Pattern p = Pattern.compile("\\d+\\s*\\d+");
        Matcher m = p.matcher(priceStr);
        if(m.find())
            price = Integer.valueOf(m.group(0).replaceAll("\\s+", ""));

        return price;
    }
}
