package testoproject.ui.pages.elements;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class FilterElement {

    private static final String EL_TYPE = "//div[@class='n-filter-block__header']//span[contains(text(), '%s')]";
    private static final String EL_IS_CLOSED = "//div[@class='n-filter-block__header']//span[contains(text(), '%s')]/../../../div[contains(@class, 'n-filter-block__body') and @style='display: none;']";

    private static final String EL_INPUT_PRICE_FROM = "//input[@name='glf-pricefrom-var']";
    private static final String EL_INPUT_PRICE_TO = "//input[@name='glf-priceto-var']";

    private static final String EL_PRODUCER = "//div/a/span/label[text()='%s']";

    private static final String EL_APPLY = "//a[contains(@class,'button_action_show-filtered')]";

    public void openByType(String type){
        boolean isClosed = $(By.xpath(String.format(EL_IS_CLOSED, type))).exists();
        if(isClosed)
            $(By.xpath(String.format(EL_TYPE, type))).click();
    }

    public void fillPrice(String from, String to){
        $(By.xpath(EL_INPUT_PRICE_FROM)).sendKeys(from);
        $(By.xpath(EL_INPUT_PRICE_TO)).sendKeys(to);
    }

    public void checkProducer(String producer){
        $(By.xpath(String.format(EL_PRODUCER, producer))).click();
    }

    public void clickApply(){
        $(By.xpath(EL_APPLY)).click();
    }
}
