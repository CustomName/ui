package testoproject.ui.pages.elements;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class SortElement {

    private static final String EL_TYPE = "//div[contains(@class,'n-filter-sorter')]/a[text()='%s']";
    private static final String EL_PRELOADER = "//div[contains(@class, 'preloader')]";

    public void sortBy(String type){
        $(By.xpath(String.format(EL_TYPE, type))).click();
        $(By.xpath(EL_PRELOADER)).waitWhile(Condition.exist, 10000);
    }
}
