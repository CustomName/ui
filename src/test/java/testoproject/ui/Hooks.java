package testoproject.ui;

import com.codeborne.selenide.Configuration;
import io.cucumber.java.Before;

public class Hooks {

    @Before
    public void beforeScenario() {
        Configuration.startMaximized = true;
        Configuration.browser = "chrome";
        Configuration.timeout = 3000;
    }
}
