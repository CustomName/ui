# language: ru
Функционал: : Сортировка

  Сценарий: Сортировка продуктов по цене
    Дано открыть страницу "https://yandex.ru"
    Тогда проверить заголовок страницы "Яндекс"
    Когда переходим в "Маркет"
    Тогда проверить заголовок страницы "Яндекс.Маркет — выбор и покупка товаров из проверенных интернет-магазинов"

    Когда выбираем категорию "Электроника"
    Тогда проверить заголовок страницы "Электроника — купить на Яндекс.Маркете"
    Когда выбираем подкатегорию "Смартфоны" в группе "Смартфоны и умные часы"

    Когда выбираем сортировку "по цене"
    Тогда проверить сортировку По цене